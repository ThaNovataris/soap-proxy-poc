package com.novataris.proxy.smile.endpoints;

import com.novataris.proxy.generate.response.ActionCompleted;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class TransactionStore {
    private static final Map<Integer, CacheRecord<String>> RESPONSE_URLS = new HashMap<>();
    private static final Map<Integer, String> METHOD_NAMES = new HashMap<>();
    private static final Map<Integer, CacheRecord<ActionCompleted>> COMPLETED_ACTIONS = new HashMap<>();

    static void putResponseUrl(Integer key, String url, String methodName) {
        RESPONSE_URLS.put(key, new CacheRecord<>(url));
        METHOD_NAMES.put(key, methodName);
    }

    static String getResponseUrl(Integer key) {
        CacheRecord<String> responseUrlRecord = RESPONSE_URLS.get(key);
        return responseUrlRecord != null ? responseUrlRecord.getRecord() : null;
    }

    static Long getCreationTime(Integer key) {
        CacheRecord<String> responseUrlRecord = RESPONSE_URLS.get(key);
        return responseUrlRecord != null ? responseUrlRecord.getCreationTime() : null;
    }

    static String getMethodName(Integer key) {
        return METHOD_NAMES.get(key);
    }

    static void putCompletedAction(Integer key, ActionCompleted completedAction) {
        COMPLETED_ACTIONS.put(key, new CacheRecord<>(completedAction));
    }

    static ActionCompleted getCompletedAction(Integer key) {
        return COMPLETED_ACTIONS.get(key).getRecord();
    }

    static long getResponseTime(Integer key) {
        return COMPLETED_ACTIONS.get(key).getCreationTime() - RESPONSE_URLS.get(key).getCreationTime();
    }

    static Set<Integer> getTransactionIds() {
        return RESPONSE_URLS.keySet();
    }

    static Set<Integer> getCompletedIds() {
        return COMPLETED_ACTIONS.keySet();
    }

    private static class CacheRecord<T> {
        private final long creationTime;
        private final T record;

        CacheRecord(T record) {
            this.creationTime = Instant.now().toEpochMilli();
            this.record = record;
        }

        public long getCreationTime() {
            return creationTime;
        }

        public T getRecord() {
            return record;
        }
    }

}
