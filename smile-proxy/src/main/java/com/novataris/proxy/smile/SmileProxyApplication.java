package com.novataris.proxy.smile;

import com.novataris.proxy.generate.ws.ObjectFactory;
import com.novataris.proxy.generate.ws.SmileX0020Webservice;
import com.novataris.proxy.generate.ws.SmileX0020WebserviceSoap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.HealthAggregator;
import org.springframework.boot.actuate.health.OrderedHealthAggregator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;

@EnableCircuitBreaker
@EnableHystrixDashboard
@EnableScheduling
@SpringBootApplication
public class SmileProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmileProxyApplication.class, args);
    }

    @Bean
    public SmileX0020WebserviceSoap portBean() {
        return new SmileX0020Webservice().getSmileX0020WebserviceSoap();
    }

    @Bean
    public ObjectFactory factoryBean() {
        return new ObjectFactory();
    }

    @Bean
    HealthAggregator healthAggregator() {
        OrderedHealthAggregator healthAggregator = new OrderedHealthAggregator();
        healthAggregator.setStatusOrder(Arrays.asList("DOWN", "OUT_OF_SERVICE", "RESTRICTED", "UP", "UNKNOWN"));

        return healthAggregator;
    }
}
