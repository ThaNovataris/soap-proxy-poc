package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.response.*;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.ws.BindingProvider;
import java.time.Duration;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000") })
public class ResponseProxyEndpoint {

    private static final String NAMESPACE_URI = "SmileResponse";
    private static final String METRIC_NAME = "response.time";
    private static final String METRIC_TAG = "smile";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ObjectFactory factory = new ObjectFactory();
    private ResponseService service = new ResponseService();

    private final MeterRegistry meterRegistry;

    @Autowired
    public ResponseProxyEndpoint(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ActionCompleted")
    @ResponsePayload
    @HystrixCommand
    public ActionCompletedResponse actionCompleted(@RequestPayload ActionCompleted request) {
        int transactionID = request.getData().getTransactionID();
        TransactionStore.putCompletedAction(transactionID, request);
        String endpointURL = TransactionStore.getResponseUrl(transactionID);
        ActionCompletedResponse response = factory.createActionCompletedResponse();
        if (endpointURL == null) {
            logger.warn("Response for unknown transaction id: " + transactionID + " received.");
            response.setActionCompletedResult(false);
        } else {
            logger.info("Forwarding response for transaction id: " + transactionID + ".");
            String methodName = TransactionStore.getMethodName(transactionID);
            Long responseTime = TransactionStore.getResponseTime(transactionID);
            meterRegistry.timer(METRIC_NAME, "Proxy", METRIC_TAG, "Method", methodName).record(Duration.ofMillis(responseTime));
            logger.info("Transaction: " + transactionID + " took " + responseTime + " ms.");
            ResponseServiceSoap responseServiceSoap = service.getResponseServiceSoap();
            ((BindingProvider) responseServiceSoap).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
            response.setActionCompletedResult(responseServiceSoap.actionCompleted(request.getData()));
        }
        return response;
    }
}
