package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.jolokia.util.IpChecker;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class IPTVProxyEndpoint extends BaseProxyEndpoint {

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_CreateFallback")
    public IPTV_CreateResponse iptv_Create(@RequestPayload IPTV_Create request) {
        IPTV_CreateResponse response = factory.createIPTV_CreateResponse();
        response.setIPTV_CreateResult(handle("IPTV_Create", request.getHost(), request.getIPTVCreate(), port::iptvCreate));
        return response;
    }

    public IPTV_CreateResponse iptv_CreateFallback(IPTV_Create request) {
        IPTV_CreateResponse response = factory.createIPTV_CreateResponse();
        response.setIPTV_CreateResult(handleFallback("IPTV_Create"));
        return response;
    }
    //endregion

    //region Delete
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_Delete")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_DeleteFallback")
    public IPTV_DeleteResponse iptv_Delete(@RequestPayload IPTV_Delete request) {
        IPTV_DeleteResponse response = factory.createIPTV_DeleteResponse();
        response.setIPTV_DeleteResult(handle("IPTV_Delete", request.getHost(), request.getCustomer(), port::iptvDelete));
        return response;
    }

    public IPTV_DeleteResponse iptv_DeleteFallback(IPTV_Delete request) {
        IPTV_DeleteResponse response = factory.createIPTV_DeleteResponse();
        response.setIPTV_DeleteResult(handleFallback("IPTV_Delete"));
        return response;
    }
    //endregion

    //region GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_GetProvisioningCodesFallback")
    public IPTV_GetProvisioningCodesResponse iptv_GetProvisioningCodes(@RequestPayload IPTV_GetProvisioningCodes request) {
        IPTV_GetProvisioningCodesResponse response = factory.createIPTV_GetProvisioningCodesResponse();
        response.setIPTV_GetProvisioningCodesResult(handleSync("IPTV_GetProvisioningCodes", request.getHost(), port::iptvGetProvisioningCodes));
        return response;
    }

    public IPTV_GetProvisioningCodesResponse iptv_GetProvisioningCodesFallback(IPTV_GetProvisioningCodes request) {
        IPTV_GetProvisioningCodesResponse response = factory.createIPTV_GetProvisioningCodesResponse();
        ResponseIPTVProvisioningCodes responseIPTVProvisioningCodes = factory.createResponseIPTVProvisioningCodes();
        responseIPTVProvisioningCodes.setDescription(handleFallbackSync("IPTV_GetProvisioningCodes"));
        response.setIPTV_GetProvisioningCodesResult(responseIPTVProvisioningCodes);
        return response;
    }
    //endregion

    //region Modify
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_Modify")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_ModifyFallback")
    public IPTV_ModifyResponse iptv_Modify(@RequestPayload IPTV_Modify request) {
        IPTV_ModifyResponse response = factory.createIPTV_ModifyResponse();
        response.setIPTV_ModifyResult(handle("IPTV_Modify", request.getHost(), request.getIPTVModify(), port::iptvModify));
        return response;
    }

    public IPTV_ModifyResponse iptv_ModifyFallback(IPTV_Modify request) {
        IPTV_ModifyResponse response = factory.createIPTV_ModifyResponse();
        response.setIPTV_ModifyResult(handleFallback("IPTV_Modify"));
        return response;
    }
    //endregion

    //region Package_Add
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_Package_Add")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_Package_AddFallback")
    public IPTV_Package_AddResponse iptv_Package_Add(@RequestPayload IPTV_Package_Add request) {
        IPTV_Package_AddResponse response = factory.createIPTV_Package_AddResponse();
        response.setIPTV_Package_AddResult(handle("IPTV_Package_Add", request.getHost(), request.getIPTVPackagesInfo(), port::iptvPackageAdd));
        return response;
    }

    public IPTV_Package_AddResponse iptv_Package_AddFallback(IPTV_Package_Add request) {
        IPTV_Package_AddResponse response = factory.createIPTV_Package_AddResponse();
        response.setIPTV_Package_AddResult(handleFallback("IPTV_Package_Add"));
        return response;
    }
    //endregion

    //region Package_Remove
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_Package_Remove")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_Package_RemoveFallback")
    public IPTV_Package_RemoveResponse iptv_Package_Remove(@RequestPayload IPTV_Package_Remove request) {
        IPTV_Package_RemoveResponse response = factory.createIPTV_Package_RemoveResponse();
        response.setIPTV_Package_RemoveResult(handle("IPTV_Package_Remove", request.getHost(), request.getIPTVPackagesInfo(), port::iptvPackageRemove));
        return response;
    }

    public IPTV_Package_RemoveResponse iptv_Package_RemoveFallback(IPTV_Package_Remove request) {
        IPTV_Package_RemoveResponse response = factory.createIPTV_Package_RemoveResponse();
        response.setIPTV_Package_RemoveResult(handleFallback("IPTV_Package_Remove"));
        return response;
    }
    //endregion

    //region VODR
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_VODR")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_VODRFallback")
    public IPTV_VODRResponse iptv_VODR(@RequestPayload IPTV_VODR request) {
        IPTV_VODRResponse response = factory.createIPTV_VODRResponse();
        response.setIPTV_VODRResult(handle("IPTV_VODR", request.getHost(), request.getIPTVVODR(), port::iptvVODR));
        return response;
    }

    public IPTV_VODRResponse iptv_VODRFallback(IPTV_VODR request) {
        IPTV_VODRResponse response = factory.createIPTV_VODRResponse();
        response.setIPTV_VODRResult(handleFallback("IPTV_VODR"));
        return response;
    }
    //endregion

    //region VODRSync
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "IPTV_VODRSync")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "iptv_VODRSyncFallback")
    public IPTV_VODRSyncResponse iptv_VODRSync(@RequestPayload IPTV_VODRSync request) {
        IPTV_VODRSyncResponse response = factory.createIPTV_VODRSyncResponse();
        response.setIPTV_VODRSyncResult(handleSync("IPTV_VODRSync", request.getHost(), request.getIPTVVODR(), port::iptvVODRSync));
        return response;
    }

    public IPTV_VODRSyncResponse iptv_VODRSyncFallback(IPTV_VODRSync request){
        IPTV_VODRSyncResponse response = factory.createIPTV_VODRSyncResponse();
        IPTVVODRData iptvvodrData = factory.createIPTVVODRData();

        ArrayOfStatusDescription descriptions = factory.createArrayOfStatusDescription();
        descriptions.getStatusDescription().add(handleFallbackSync("IPTV_VODRSync"));
        iptvvodrData.setDescriptions(descriptions);

        response.setIPTV_VODRSyncResult(iptvvodrData);
        return response;
    }
    //endregion

}
