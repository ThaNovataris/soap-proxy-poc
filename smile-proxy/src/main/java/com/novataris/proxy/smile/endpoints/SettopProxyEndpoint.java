package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class SettopProxyEndpoint extends BaseProxyEndpoint {

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SettopBox_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "settopBox_CreateFallback")
    public SettopBox_CreateResponse settopBox_Create(@RequestPayload SettopBox_Create request) {
        SettopBox_CreateResponse response = factory.createSettopBox_CreateResponse();
        response.setSettopBox_CreateResult(handle("SettopBox_Create", request.getHost(), request.getSettopBoxInfo(), port::settopBoxCreate));
        return response;
    }

    public SettopBox_CreateResponse settopBox_CreateFallback(SettopBox_Create request) {
        SettopBox_CreateResponse response = factory.createSettopBox_CreateResponse();
        response.setSettopBox_CreateResult(handleFallback("SettopBox_Create"));
        return response;
    }
    //endregion

    //region Delete
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SettopBox_Delete")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "settopBox_DeleteFallback")
    public SettopBox_DeleteResponse settopBox_Delete(@RequestPayload SettopBox_Delete request) {
        SettopBox_DeleteResponse response = factory.createSettopBox_DeleteResponse();
        response.setSettopBox_DeleteResult(handle("SettopBox_Delete", request.getHost(), request.getSettopBoxInfo(), port::settopBoxDelete));
        return response;
    }

    public SettopBox_DeleteResponse settopBox_DeleteFallback(SettopBox_Delete request) {
        SettopBox_DeleteResponse response = factory.createSettopBox_DeleteResponse();
        response.setSettopBox_DeleteResult(handleFallback("SettopBox_Delete"));
        return response;
    }
    //endregion

    //region GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SettopBox_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "settopBox_GetProvisioningCodesFallback")
    public SettopBox_GetProvisioningCodesResponse settopBox_GetProvisioningCodes(@RequestPayload SettopBox_GetProvisioningCodes request) {
        SettopBox_GetProvisioningCodesResponse response = factory.createSettopBox_GetProvisioningCodesResponse();
        response.setSettopBox_GetProvisioningCodesResult(handleSync("SettopBox_GetProvisioningCodes", request.getHost(), port::settopBoxGetProvisioningCodes));
        return response;
    }

    public SettopBox_GetProvisioningCodesResponse settopBox_GetProvisioningCodesFallback(SettopBox_GetProvisioningCodes request) {
        SettopBox_GetProvisioningCodesResponse response = factory.createSettopBox_GetProvisioningCodesResponse();
        ResponseSettopProvisioningCodes responseSettopProvisioningCodes = factory.createResponseSettopProvisioningCodes();
        responseSettopProvisioningCodes.setDescription(handleFallbackSync("SettopBox_GetProvisioningCodes"));
        response.setSettopBox_GetProvisioningCodesResult(responseSettopProvisioningCodes);
        return response;
    }
    //endregion

}
