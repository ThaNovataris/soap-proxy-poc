package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class ForeningProxyEndpoint extends BaseProxyEndpoint {

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Forening_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "forening_CreateFallback")
    public Forening_CreateResponse forening_Create(@RequestPayload Forening_Create request) {
        Forening_CreateResponse response = factory.createForening_CreateResponse();
        response.setForening_CreateResult(handle("Forening_Create", request.getHost(), request.getForeningInfo(), port::foreningCreate));
        return response;
    }

    public Forening_CreateResponse forening_CreateFallback(Forening_Create request) {
        Forening_CreateResponse response = factory.createForening_CreateResponse();
        response.setForening_CreateResult(handleFallback("Forening_Create"));
        return response;
    }
    //endregion

    //region GetList
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Forening_GetList")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "forening_GetListFallback")
    public Forening_GetListResponse forening_GetList(@RequestPayload Forening_GetList request) {
        Forening_GetListResponse response = factory.createForening_GetListResponse();
        response.setForening_GetListResult(handle("Forening_GetList", request.getHost(), port::foreningGetList));
        return response;
    }

    public Forening_GetListResponse forening_GetListFallback(Forening_GetList request) {
        Forening_GetListResponse response = factory.createForening_GetListResponse();
        response.setForening_GetListResult(handleFallback("Forening_GetList"));
        return response;
    }
    //endregion

    //region Modify
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Forening_Modify")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "forening_ModifyFallback")
    public Forening_ModifyResponse forening_Modify(@RequestPayload Forening_Modify request) {
        Forening_ModifyResponse response = factory.createForening_ModifyResponse();
        response.setForening_ModifyResult(handle("Forening_Modify", request.getHost(), request.getForeningInfo(), port::foreningModify));
        return response;
    }

    public Forening_ModifyResponse forening_ModifyFallback(Forening_Modify request) {
        Forening_ModifyResponse response = factory.createForening_ModifyResponse();
        response.setForening_ModifyResult(handleFallback("Forening_Modify"));
        return response;
    }
    //endregion
}
