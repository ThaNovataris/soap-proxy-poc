package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class CustomerProxyEndpoint extends BaseProxyEndpoint {

    //region AddPermissions
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_AddPermissions")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_AddPermissionsFallback")
    public Customer_AddPermissionsResponse customer_AddPermissions(@RequestPayload Customer_AddPermissions request) {
        Customer_AddPermissionsResponse response = factory.createCustomer_AddPermissionsResponse();
        response.setCustomer_AddPermissionsResult(handle("Customer_AddPermissions", request.getHost(), request.getCustomer(), port::customerAddPermissions));
        return response;
    }

    public Customer_AddPermissionsResponse customer_AddPermissionsFallback(Customer_AddPermissions request) {
        Customer_AddPermissionsResponse response = factory.createCustomer_AddPermissionsResponse();
        response.setCustomer_AddPermissionsResult(handleFallback("Customer_AddPermissions"));
        return response;
    }
    //endregion

    //region ChangePricelist
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_ChangePricelist")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_ChangePricelistFallback")
    public Customer_ChangePricelistResponse customer_ChangePricelist(@RequestPayload Customer_ChangePricelist request) {
        Customer_ChangePricelistResponse response = factory.createCustomer_ChangePricelistResponse();
        response.setCustomer_ChangePricelistResult(handleSync("Customer_ChangePricelist", request.getHost(), request.getCustomer(), port::customerChangePricelist));
        return response;
    }

    public Customer_ChangePricelistResponse customer_ChangePricelistFallback(Customer_ChangePricelist request) {
        Customer_ChangePricelistResponse response = factory.createCustomer_ChangePricelistResponse();
        ResponseCustomerChangePricelist responseCustomerChangePricelist = factory.createResponseCustomerChangePricelist();
        responseCustomerChangePricelist.setDescription(handleFallbackSync("Customer_ChangePricelist"));
        response.setCustomer_ChangePricelistResult(responseCustomerChangePricelist);
        return response;
    }
    //endregion

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_CreateFallback")
    public Customer_CreateResponse customer_Create(@RequestPayload Customer_Create request) {
        Customer_CreateResponse response = factory.createCustomer_CreateResponse();
        response.setCustomer_CreateResult(handle("Customer_Create", request.getHost(), request.getCustomer(), port::customerCreate));
        return response;
    }

    public Customer_CreateResponse customer_CreateFallback(Customer_Create request) {
        Customer_CreateResponse response = factory.createCustomer_CreateResponse();
        response.setCustomer_CreateResult(handleFallback("Customer_Create"));
        return response;
    }
    //endregion

    //region CreateWithPricelist
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_CreateWithPricelist")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_CreateWithPricelistFallback")
    public Customer_CreateWithPricelistResponse customer_CreateWithPricelist(@RequestPayload Customer_CreateWithPricelist request) {
        Customer_CreateWithPricelistResponse response = factory.createCustomer_CreateWithPricelistResponse();
        response.setCustomer_CreateWithPricelistResult(handle("Customer_CreateWithPricelist", request.getHost(), request.getCustomer(), port::customerCreateWithPricelist));
        return response;
    }

    public Customer_CreateWithPricelistResponse customer_CreateWithPricelistFallback(Customer_CreateWithPricelist request) {
        Customer_CreateWithPricelistResponse response = factory.createCustomer_CreateWithPricelistResponse();
        response.setCustomer_CreateWithPricelistResult(handleFallback("Customer_CreateWithPricelist"));
        return response;
    }
    //endregion

    //region Deactivate
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_Deactivate")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_DeactivateFallback")
    public Customer_DeactivateResponse customer_Deactivate(@RequestPayload Customer_Deactivate request) {
        Customer_DeactivateResponse response = factory.createCustomer_DeactivateResponse();
        response.setCustomer_DeactivateResult(handle("Customer_Deactivate", request.getHost(), request.getCustomer(), port::customerDeactivate));
        return response;
    }

    public Customer_DeactivateResponse customer_DeactivateFallback(Customer_Deactivate request) {
        Customer_DeactivateResponse response = factory.createCustomer_DeactivateResponse();
        response.setCustomer_DeactivateResult(handleFallback("Customer_Deactivate"));
        return response;
    }
    //endregion

    //region Modify
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_Modify")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_ModifyFallback")
    public Customer_ModifyResponse customer_Modify(@RequestPayload Customer_Modify request) {
        Customer_ModifyResponse response = factory.createCustomer_ModifyResponse();
        response.setCustomer_ModifyResult(handle("Customer_Modify", request.getHost(), request.getCustomer(), port::customerModify));
        return response;
    }

    public Customer_ModifyResponse customer_ModifyFallback(Customer_Modify request) {
        Customer_ModifyResponse response = factory.createCustomer_ModifyResponse();
        response.setCustomer_ModifyResult(handleFallback("Customer_Modify"));
        return response;
    }
    //endregion

    //region Modify_Forening
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_Modify_Forening")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_Modify_ForeningFallback")
    public Customer_Modify_ForeningResponse customer_Modify_Forening(@RequestPayload Customer_Modify_Forening request) {
        Customer_Modify_ForeningResponse response = factory.createCustomer_Modify_ForeningResponse();
        response.setCustomer_Modify_ForeningResult(handle("Customer_Modify_Forening", request.getHost(), request.getCustomerforening(), port::customerModifyForening));
        return response;
    }

    public Customer_Modify_ForeningResponse customer_Modify_ForeningFallback(Customer_Modify_Forening request) {
        Customer_Modify_ForeningResponse response = factory.createCustomer_Modify_ForeningResponse();
        response.setCustomer_Modify_ForeningResult(handleFallback("Customer_Modify_Forening"));
        return response;
    }
    //endregion

    //region Query
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Customer_Query")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "customer_QueryFallback")
    public Customer_QueryResponse customer_Query(@RequestPayload Customer_Query request) {
        Customer_QueryResponse response = factory.createCustomer_QueryResponse();
        response.setCustomer_QueryResult(handle("Customer_Query", request.getHost(), request.getCustomer(), port::customerQuery));
        return response;
    }

    public Customer_QueryResponse customer_QueryFallback(Customer_Query request) {
        Customer_QueryResponse response = factory.createCustomer_QueryResponse();
        response.setCustomer_QueryResult(handleFallback("Customer_Query"));
        return response;
    }
    //endregion
}
