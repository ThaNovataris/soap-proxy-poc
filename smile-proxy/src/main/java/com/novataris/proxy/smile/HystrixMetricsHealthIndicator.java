package com.novataris.proxy.smile;

import com.netflix.hystrix.HystrixCircuitBreaker;
import com.netflix.hystrix.HystrixCommandMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
class HystrixMetricsHealthIndicator extends AbstractHealthIndicator {

    private final Environment env;

    @Autowired
    public HystrixMetricsHealthIndicator(Environment env) {
        this.env = env;
    }

    private List<String> getCriticalEndpoints() {
        String envProperty = env.getProperty("hystrix.critial.endpoints");
        return envProperty != null ? Arrays.asList(envProperty.split(",")) : new ArrayList<>();
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        List<String> breakers = new ArrayList<>();
        List<String> criticalBreakers = new ArrayList<>();
        HystrixCommandMetrics.getInstances().forEach(instance -> {
            HystrixCircuitBreaker breaker = HystrixCircuitBreaker.Factory.getInstance(instance.getCommandKey());
            if (breaker.isOpen()) {
                String breakerName = instance.getCommandGroup().name() + "::" + instance.getCommandKey().name();
                breakers.add(breakerName);
                if (getCriticalEndpoints().contains(breakerName)) {
                    criticalBreakers.add(breakerName);
                }
            }
        });
        if (!criticalBreakers.isEmpty()) {
            builder.outOfService().withDetail("criticalOpenBreakers", criticalBreakers).withDetail("openCircuitBreakers", breakers);
        } else if (!breakers.isEmpty()) {
            builder.status("RESTRICTED").withDetail("openCircuitBreakers", breakers);
        } else {
            builder.up();
        }
    }
}