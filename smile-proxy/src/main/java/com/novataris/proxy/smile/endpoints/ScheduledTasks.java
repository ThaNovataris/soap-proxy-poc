package com.novataris.proxy.smile.endpoints;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final long DEFAULT_THRESHOLD = 300000;

    private final Environment env;

    @Autowired
    public ScheduledTasks(Environment env) {
        this.env = env;
    }

    // Default value is 300.000 milliseconds = 5 minutes.
    @Scheduled(fixedDelayString = "${oldTransactions.fixedDelay:10000}")
    public void handleOldTasks() {
        logger.debug("Handling old tasks...");
        Set<Integer>
                incompleteTransactions =
                TransactionStore.getTransactionIds()
                        .stream()
                        .filter(key -> !TransactionStore.getCompletedIds().contains(key))
                        .collect(Collectors.toSet());
        @SuppressWarnings("ConstantConditions")
        // Keys have been retrieved in line above, hence 'getCreationTime()' should never be able to return null.
        Set<Integer>
                oldTransactions =
                incompleteTransactions.stream()
                        .filter(key -> Instant.now().toEpochMilli() - TransactionStore.getCreationTime(key) >= getThreshold())
                        .collect(Collectors.toSet());

        // TODO: Do something with old transactions.
        logger.debug("oldTransactions.size(): " + oldTransactions.size());
    }

    private Long getThreshold() {
        String thresholdProp = env.getProperty("oldTransactions.threshold");
        return thresholdProp != null ? Long.parseLong(thresholdProp) : DEFAULT_THRESHOLD;
    }
}
