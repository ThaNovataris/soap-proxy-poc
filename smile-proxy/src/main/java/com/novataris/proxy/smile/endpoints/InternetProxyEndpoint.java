package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class InternetProxyEndpoint extends BaseProxyEndpoint {

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Internet_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "internet_CreateFallback")
    public Internet_CreateResponse internet_Create(@RequestPayload Internet_Create request) {
        Internet_CreateResponse response = factory.createInternet_CreateResponse();
        response.setInternet_CreateResult(handle("Internet_Create", request.getHost(), request.getInternetInfo(), port::internetCreate));
        return response;
    }

    public Internet_CreateResponse internet_CreateFallback(Internet_Create request) {
        Internet_CreateResponse response = factory.createInternet_CreateResponse();
        response.setInternet_CreateResult(handleFallback("Internet_Create"));
        return response;
    }
    //endregion

    //region Delete
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Internet_Delete")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "internet_DeleteFallback")
    public Internet_DeleteResponse internet_Delete(@RequestPayload Internet_Delete request) {
        Internet_DeleteResponse response = factory.createInternet_DeleteResponse();
        response.setInternet_DeleteResult(handle("Internet_Delete", request.getHost(), request.getInternetInfo(), port::internetDelete));
        return response;
    }

    public Internet_DeleteResponse internet_DeleteFallback(Internet_Delete request) {
        Internet_DeleteResponse response = factory.createInternet_DeleteResponse();
        response.setInternet_DeleteResult(handleFallback("Internet_Delete"));
        return response;
    }
    //endregion

    //region GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Internet_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "internet_GetProvisioningCodesFallback")
    public Internet_GetProvisioningCodesResponse internet_GetProvisioningCodes(@RequestPayload Internet_GetProvisioningCodes request) {
        Internet_GetProvisioningCodesResponse response = factory.createInternet_GetProvisioningCodesResponse();
        response.setInternet_GetProvisioningCodesResult(handleSync("Internet_GetProvisioningCodes", request.getHost(), port::internetGetProvisioningCodes));
        return response;
    }

    public Internet_GetProvisioningCodesResponse internet_GetProvisioningCodesFallback(Internet_GetProvisioningCodes request) {
        Internet_GetProvisioningCodesResponse response = factory.createInternet_GetProvisioningCodesResponse();
        ResponseInternetProvisioningCodes responseInternetProvisioningCodes = factory.createResponseInternetProvisioningCodes();
        responseInternetProvisioningCodes.setDescription(handleFallbackSync("Internet_GetProvisioningCodes"));
        response.setInternet_GetProvisioningCodesResult(responseInternetProvisioningCodes);
        return response;
    }
    //endregion

    //region Modify
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Internet_Modify")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "internet_ModifyFallback")
    public Internet_ModifyResponse internet_Modify(@RequestPayload Internet_Modify request) {
        Internet_ModifyResponse response = factory.createInternet_ModifyResponse();
        response.setInternet_ModifyResult(handle("Internet_Modify", request.getHost(), request.getInternetInfo(), port::internetModify));
        return response;
    }

    public Internet_ModifyResponse internet_ModifyFallback(Internet_Modify request) {
        Internet_ModifyResponse response = factory.createInternet_ModifyResponse();
        response.setInternet_ModifyResult(handleFallback("Internet_Modify"));
        return response;
    }
    //endregion

    //region Services_GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Internet_Services_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "internet_Services_GetProvisioningCodesFallback")
    public Internet_Services_GetProvisioningCodesResponse internet_Services_GetProvisioningCodes(@RequestPayload Internet_Services_GetProvisioningCodes request) {
        Internet_Services_GetProvisioningCodesResponse response = factory.createInternet_Services_GetProvisioningCodesResponse();
        response.setInternet_Services_GetProvisioningCodesResult(handleSync("Internet_Services_GetProvisioningCodes", request.getHost(), request.getInternetProvisioningCode(), port::internetServicesGetProvisioningCodes));
        return response;
    }

    public Internet_Services_GetProvisioningCodesResponse internet_Services_GetProvisioningCodesFallback(Internet_Services_GetProvisioningCodes request){
        Internet_Services_GetProvisioningCodesResponse response = factory.createInternet_Services_GetProvisioningCodesResponse();
        ResponseInternetProvisioningCodes responseInternetProvisioningCodes = factory.createResponseInternetProvisioningCodes();
        responseInternetProvisioningCodes.setDescription(handleFallbackSync("Internet_Services_GerProvisioningCodes"));
        response.setInternet_Services_GetProvisioningCodesResult(responseInternetProvisioningCodes);
        return response;
    }
    //endregion
}
