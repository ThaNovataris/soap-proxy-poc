package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import sun.management.VMOptionCompositeData;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class VOIPProxyEndpoint extends BaseProxyEndpoint {

    //region CDR
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_CDR")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_CDRFallback")
    public VOIP_CDRResponse voip_CDR(@RequestPayload VOIP_CDR request) {
        VOIP_CDRResponse response = factory.createVOIP_CDRResponse();
        response.setVOIP_CDRResult(handle("VOIP_CDR", request.getHost(), request.getVOIPCDR(), port::voipCDR));
        return response;
    }

    public VOIP_CDRResponse voip_CDRFallback(VOIP_CDR request) {
        VOIP_CDRResponse response = factory.createVOIP_CDRResponse();
        response.setVOIP_CDRResult(handleFallback("VOIP_CDR"));
        return response;
    }
    //endregion

    //region CDRSync
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_CDRSync")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_CDRSyncFallback")
    public VOIP_CDRSyncResponse voip_CDRSync(@RequestPayload VOIP_CDRSync request) {
        VOIP_CDRSyncResponse response = factory.createVOIP_CDRSyncResponse();
        response.setVOIP_CDRSyncResult(handleSync("VOIP_CDRSync", request.getHost(), request.getVOIPCDR(), port::voipCDRSync));
        return response;
    }

    public VOIP_CDRSyncResponse voip_CDRSyncFallback(VOIP_CDRSync request) {
        VOIP_CDRSyncResponse response = factory.createVOIP_CDRSyncResponse();
        CDRData cdrData = factory.createCDRData();

        ArrayOfStatusDescription descriptions = factory.createArrayOfStatusDescription();
        descriptions.getStatusDescription().add(handleFallbackSync("VOIP_CDRSync"));
        cdrData.setDescriptions(descriptions);
        response.setVOIP_CDRSyncResult(cdrData);
        return response;
    }
    //endregion

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_CreateFallback")
    public VOIP_CreateResponse voip_Create(@RequestPayload VOIP_Create request) {
        VOIP_CreateResponse response = factory.createVOIP_CreateResponse();
        response.setVOIP_CreateResult(handle("VOIP_Create", request.getHost(), request.getIpPhoneInfo(), port::voipCreate));
        return response;
    }

    public VOIP_CreateResponse voip_CreateFallback(VOIP_Create request) {
        VOIP_CreateResponse response = factory.createVOIP_CreateResponse();
        response.setVOIP_CreateResult(handleFallback("VOIP_Create"));
        return response;
    }
    //endregion

    //region Delete
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_Delete")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_DeleteFallback")
    public VOIP_DeleteResponse voip_Delete(@RequestPayload VOIP_Delete request) {
        VOIP_DeleteResponse response = factory.createVOIP_DeleteResponse();
        response.setVOIP_DeleteResult(handle("VOIP_Delete", request.getHost(), request.getIpPhoneInfo(), port::voipDelete));
        return response;
    }

    public VOIP_DeleteResponse voip_DeleteFallback(VOIP_Delete request) {
        VOIP_DeleteResponse response = factory.createVOIP_DeleteResponse();
        response.setVOIP_DeleteResult(handleFallback("VOIP_Delete"));
        return response;
    }
    //endregion

    //region GetCompanyProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_GetCompanyProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_GetCompanyProvisioningCodesFallback")
    public VOIP_GetCompanyProvisioningCodesResponse voip_GetCompanyProvisioningCodes(@RequestPayload VOIP_GetCompanyProvisioningCodes request) {
        VOIP_GetCompanyProvisioningCodesResponse response = factory.createVOIP_GetCompanyProvisioningCodesResponse();
        response.setVOIP_GetCompanyProvisioningCodesResult(handleSync("VOIP_GetCompanyProvisioningCodes", request.getHost(), port::voipGetCompanyProvisioningCodes));
        return response;
    }

    public VOIP_GetCompanyProvisioningCodesResponse voip_GetCompanyProvisioningCodesFallback(VOIP_GetCompanyProvisioningCodes request) {
        VOIP_GetCompanyProvisioningCodesResponse response = factory.createVOIP_GetCompanyProvisioningCodesResponse();
        ResponseVOIPCompanyCodes responseVOIPCompanyCodes = factory.createResponseVOIPCompanyCodes();
        responseVOIPCompanyCodes.setDescription(handleFallbackSync("VOIP_GetCompanyProvisioningCodes"));
        response.setVOIP_GetCompanyProvisioningCodesResult(responseVOIPCompanyCodes);
        return response;
    }
    //endregion

    //region GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_GetProvisioningCodesFallback")
    public VOIP_GetProvisioningCodesResponse voip_GetProvisioningCodes(@RequestPayload VOIP_GetProvisioningCodes request) {
        VOIP_GetProvisioningCodesResponse response = factory.createVOIP_GetProvisioningCodesResponse();
        response.setVOIP_GetProvisioningCodesResult(handleSync("VOIP_GetProvisioningCodes", request.getHost(), port::voipGetProvisioningCodes));
        return response;
    }

    public VOIP_GetProvisioningCodesResponse voip_GetProvisioningCodesFallback(VOIP_GetProvisioningCodes request) {
        VOIP_GetProvisioningCodesResponse response = factory.createVOIP_GetProvisioningCodesResponse();
        ResponseVOIPProvisioningCodes responseVOIPProvisioningCodes = factory.createResponseVOIPProvisioningCodes();
        responseVOIPProvisioningCodes.setDescription(handleFallbackSync("VOIP_GetProvisioningCodes"));
        response.setVOIP_GetProvisioningCodesResult(responseVOIPProvisioningCodes);
        return response;
    }
    //endregion

    //region Modify
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_Modify")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_ModifyFallback")
    public VOIP_ModifyResponse voip_Modify(@RequestPayload VOIP_Modify request) {
        VOIP_ModifyResponse response = factory.createVOIP_ModifyResponse();
        response.setVOIP_ModifyResult(handle("VOIP_Modify", request.getHost(), request.getIpPhoneInfo(), port::voipModify));
        return response;
    }

    public VOIP_ModifyResponse voip_ModifyFallback(VOIP_Modify request) {
        VOIP_ModifyResponse response = factory.createVOIP_ModifyResponse();
        response.setVOIP_ModifyResult(handleFallback("VOIP_Modify"));
        return response;
    }
    //endregion

    //region Services_GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "VOIP_Services_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "voip_Services_GetProvisioningCodesFallback")
    public VOIP_Services_GetProvisioningCodesResponse voip_Services_GetProvisioningCodes(@RequestPayload VOIP_Services_GetProvisioningCodes request){
        VOIP_Services_GetProvisioningCodesResponse response = factory.createVOIP_Services_GetProvisioningCodesResponse();
        response.setVOIP_Services_GetProvisioningCodesResult(handleSync("VOIP_Services_GetProvisioningCodes", request.getHost(), request.getVOIPProvisioningCode(), port::voipServicesGetProvisioningCodes));
        return response;
    }

    public VOIP_Services_GetProvisioningCodesResponse voip_Services_GetProvisioningCodesFallback(VOIP_Services_GetProvisioningCodes request) {
        VOIP_Services_GetProvisioningCodesResponse response = factory.createVOIP_Services_GetProvisioningCodesResponse();
        ResponseVOIPProvisioningCodes responseVOIPProvisioningCodes = factory.createResponseVOIPProvisioningCodes();

        responseVOIPProvisioningCodes.setDescription(handleFallbackSync("VOIP_Services_GetProvisioningCodes"));
        response.setVOIP_Services_GetProvisioningCodesResult(responseVOIPProvisioningCodes);
        return response;
    }
    //endregion

}
