package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class WifiProxyEndpoint extends BaseProxyEndpoint {

    //region Create
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Wifi_Create")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "wifi_CreateFallback")
    public Wifi_CreateResponse wifi_Create(@RequestPayload Wifi_Create request) {
        Wifi_CreateResponse response = factory.createWifi_CreateResponse();
        response.setWifi_CreateResult(handle("Wifi_Create", request.getHost(), request.getWifiBoxInfo(), port::wifiCreate));
        return response;
    }

    public Wifi_CreateResponse wifi_CreateFallback(Wifi_Create request) {
        Wifi_CreateResponse response = factory.createWifi_CreateResponse();
        response.setWifi_CreateResult(handleFallback("Wifi_Create"));
        return response;
    }
    //endregion

    //region Delete
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Wifi_Delete")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "wifi_DeleteFallback")
    public Wifi_DeleteResponse wifi_Delete(@RequestPayload Wifi_Delete request) {
        Wifi_DeleteResponse response = factory.createWifi_DeleteResponse();
        response.setWifi_DeleteResult(handle("Wifi_Delete", request.getHost(), request.getWifiBoxInfo(), port::wifiDelete));
        return response;
    }

    public Wifi_DeleteResponse wifi_DeleteFallback(Wifi_Delete request) {
        Wifi_DeleteResponse response = factory.createWifi_DeleteResponse();
        response.setWifi_DeleteResult(handleFallback("Wifi_Delete"));
        return response;
    }
    //endregion

    //region GetProvisioningCodes
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Wifi_GetProvisioningCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "wifi_GetProvisioningCodesFallback")
    public Wifi_GetProvisioningCodesResponse wifi_GetProvisioningCodes(@RequestPayload Wifi_GetProvisioningCodes request) {
        Wifi_GetProvisioningCodesResponse response = factory.createWifi_GetProvisioningCodesResponse();
        response.setWifi_GetProvisioningCodesResult(handleSync("Wifi_GetProvisioningCodes", request.getHost(), port::wifiGetProvisioningCodes));
        return response;
    }

    public Wifi_GetProvisioningCodesResponse wifi_GetProvisioningCodesFallback(Wifi_GetProvisioningCodes request) {
        Wifi_GetProvisioningCodesResponse response = factory.createWifi_GetProvisioningCodesResponse();
        ResponseWifiProvisioningCodes responseWifiProvisioningCodes = factory.createResponseWifiProvisioningCodes();

        responseWifiProvisioningCodes.setDescription(handleFallbackSync("Wifi_Delete"));
        response.setWifi_GetProvisioningCodesResult(responseWifiProvisioningCodes);
        return response;
    }
    //endregion

}
