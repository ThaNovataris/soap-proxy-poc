package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.WifiBox_Add;
import com.novataris.proxy.generate.ws.WifiBox_AddResponse;
import com.novataris.proxy.generate.ws.WifiBox_Remove;
import com.novataris.proxy.generate.ws.WifiBox_RemoveResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class WifiBoxProxyEndpoint extends BaseProxyEndpoint {

    //region Add
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "WifiBox_Add")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "wifiBox_AddFallback")
    public WifiBox_AddResponse wifiBox_Add(@RequestPayload WifiBox_Add request) {
        WifiBox_AddResponse response = factory.createWifiBox_AddResponse();
        response.setWifiBox_AddResult(handle("WifiBox_Add", request.getHost(), request.getWifiBoxAdd(), port::wifiBoxAdd));
        return response;
    }

    public WifiBox_AddResponse wifiBox_AddFallback(WifiBox_Add request) {
        WifiBox_AddResponse response = factory.createWifiBox_AddResponse();
        response.setWifiBox_AddResult(handleFallback("WifiBox_Add"));
        return response;
    }
    //endregion

    //region Remove
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "WifiBox_Remove")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "wifiBox_RemoveFallback")
    public WifiBox_RemoveResponse wifiBox_Remove(@RequestPayload WifiBox_Remove request) {
        WifiBox_RemoveResponse response = factory.createWifiBox_RemoveResponse();
        response.setWifiBox_RemoveResult(handle("WifiBox_Remove", request.getHost(), request.getWifiBoxRemove(), port::wifiBoxRemove));
        return response;
    }

    public WifiBox_RemoveResponse wifiBox_RemoveFallback(WifiBox_Remove request) {
        WifiBox_RemoveResponse response = factory.createWifiBox_RemoveResponse();
        response.setWifiBox_RemoveResult(handleFallback("WifiBox_Remove"));
        return response;
    }
    //endregion

}
