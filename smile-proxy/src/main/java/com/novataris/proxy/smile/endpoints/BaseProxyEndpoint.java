package com.novataris.proxy.smile.endpoints;

import com.novataris.proxy.generate.ws.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

public abstract class BaseProxyEndpoint {
    protected static final String NAMESPACE_URI = "Smile";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final Collection<Integer> errorStatusCodes = Arrays.asList(2, 4, 5, 6, 11, 12, 13, 14, 15, 16, 17, 18, 51);

    @Autowired
    protected SmileX0020WebserviceSoap port;
    @Autowired
    protected ObjectFactory factory;
    @Autowired
    private Environment env;

    private String getResponseUrl() {
        return env.getProperty("proxy.address") + "smilesemiopen.asmx";
    }

    <T> Response handle(String requestName, Host host, T request, BiFunction<Host, T, Response> handler) {
        logger.info("Received \"" + requestName + "\" request");;
        String originalResponseUrl = null;
        if (host != null) {
            logger.debug(String.format("Set response URL to: \"%s\"", getResponseUrl()));
            originalResponseUrl = host.getResponseUrl();
            host.setResponseUrl(getResponseUrl());
        }
        Response response = handler.apply(host, request);
        int transactionID = response.getTransactionID();
        if (originalResponseUrl != null) {
            TransactionStore.putResponseUrl(transactionID, originalResponseUrl, requestName);
        }
        return response;
    }

    <T> Response handle(String requestName, Host host, Function<Host, Response> handler) {
        logger.info("Received \"" + requestName + "\" request");
        String originalResponseUrl = null;
        if (host != null) {
            logger.debug(String.format("Set response URL to: \"%s\"", getResponseUrl()));
            originalResponseUrl = host.getResponseUrl();
            host.setResponseUrl(getResponseUrl());
        }
        Response response = handler.apply(host);
        int transactionID = response.getTransactionID();
        if (originalResponseUrl != null && !errorStatusCodes.contains(transactionID)) {
            TransactionStore.putResponseUrl(transactionID, originalResponseUrl, requestName);
        }
        return response;
    }

    <T> T handleSync(String requestName, Host host, Function<Host, T> handler) {
        logger.info("Received \"" + requestName + "\" request");
        if (host != null) {
            host.setResponseUrl(getResponseUrl());
        }
        return handler.apply(host);
    }

    <T, U> U handleSync(String requestName, Host host, T request, BiFunction<Host, T, U> handler) {
        logger.info("Received \"" + requestName + "\" request");
        if (host != null) {
            host.setResponseUrl(getResponseUrl());
        }
        return handler.apply(host, request);
    }

    Response handleFallback(String requestName) {
        logger.warn(String.format("\"%s\" request failed - invoking fallback", requestName));
        return serviceUnavailableResponse();
    }

    StatusDescription handleFallbackSync(String requestName) {
        logger.warn(String.format("\"%s\" request failed - invoking fallback", requestName));
        return serviceUnavailableDescription();
    }

    private StatusDescription serviceUnavailableDescription() {
        StatusDescription statusDescription = factory.createStatusDescription();
        statusDescription.setStatusCode(100);
        statusDescription.setDescription("Service unavailable - try again later");
        return statusDescription;
    }

    private Response serviceUnavailableResponse() {
        Response response = factory.createResponse();
        ArrayOfStatusDescription descriptions = factory.createArrayOfStatusDescription();
        descriptions.getStatusDescription().add(serviceUnavailableDescription());
        response.setDescriptions(descriptions);
        return response;
    }
}
