package com.novataris.proxy.smile.endpoints;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.novataris.proxy.generate.ws.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.function.BiFunction;

@Endpoint
@DefaultProperties(commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
public class GetPriceListCodesProxyEndpoint extends BaseProxyEndpoint {

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetPriceListCodes")
    @ResponsePayload
    @HystrixCommand(fallbackMethod = "getPriceListCodesFallback")
    public GetPriceListCodesResponse getPriceListCodes(@RequestPayload GetPriceListCodes request) {
        GetPriceListCodesResponse response = factory.createGetPriceListCodesResponse();
        response.setGetPriceListCodesResult(handleSync("GetPriceListCodes", request.getHost(), port::getPriceListCodes));
        return response;
    }

    public GetPriceListCodesResponse getPriceListCodesFallback(GetPriceListCodes request) {
        GetPriceListCodesResponse response = factory.createGetPriceListCodesResponse();
        ResponsePriceListProvisioningCodes responsePriceListProvisioningCodes = factory.createResponsePriceListProvisioningCodes();
        responsePriceListProvisioningCodes.setDescription(handleFallbackSync("GetPriceListCodes"));
        response.setGetPriceListCodesResult(responsePriceListProvisioningCodes);
        return response;
    }
}
