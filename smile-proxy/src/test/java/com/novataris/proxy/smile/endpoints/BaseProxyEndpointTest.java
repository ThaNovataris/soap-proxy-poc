package com.novataris.proxy.smile.endpoints;

import com.novataris.proxy.generate.ws.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@SuppressWarnings("JavaDoc")
@RunWith(MockitoJUnitRunner.class)
public class BaseProxyEndpointTest {

    // Assertion Constants
    private static final int SERVICE_UNAVAILABLE_STATUS_CODE = 100;
    private static final int MOCK_TRANSACTION_ID = 5;
    private static final String RESPONSE_URL = "real-response-url";

    @Mock
    private SmileX0020WebserviceSoap mockPort;
    @Spy
    private ObjectFactory factory;
    @Mock
    private Environment environment;

    @InjectMocks
    private BaseProxyEndpoint proxyEndpoint = new BaseProxyEndpoint() {
    };

    @Test
    public void handleBiFunction() {
        // Arrange
        Customer_Create request = factory.createCustomer_Create();

        Host host = factory.createHost();
        host.setResponseUrl(RESPONSE_URL);
        host.setUser("Test");
        host.setPassword("tseT");
        request.setHost(host);

        Response mockResponse = factory.createResponse();
        mockResponse.setTransactionID(MOCK_TRANSACTION_ID);
        when(mockPort.customerCreate(eq(host), any())).thenReturn(mockResponse);

        String proxyUrl = "proxy-response-url/";
        when(environment.getProperty("proxy.address")).thenReturn(proxyUrl);

        // Act
        proxyEndpoint.handle("handleTest", request.getHost(), request.getCustomer(), mockPort::customerCreate);

        // Assert
        verify(mockPort).customerCreate(eq(host), any());
        assertEquals(RESPONSE_URL, TransactionStore.getResponseUrl(MOCK_TRANSACTION_ID));
        assertEquals(proxyUrl + "smilesemiopen.asmx", host.getResponseUrl());
    }

    @Test
    public void handleBiFunction_nullHost() {
        // Arrange
        Customer_Create request = factory.createCustomer_Create();
        Response mockResponse = factory.createResponse();
        when(mockPort.customerCreate(eq(null), any())).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handle("handleTest", request.getHost(), request.getCustomer(), mockPort::customerCreate);

        // Assert
        verify(mockPort).customerCreate(eq(null), any());
    }

    @Test
    public void handleFunction() {
        // Arrange
        Forening_GetList request = factory.createForening_GetList();

        Host host = factory.createHost();
        host.setResponseUrl("#");
        host.setUser("Test");
        host.setPassword("tseT");
        request.setHost(host);

        Response mockResponse = factory.createResponse();
        when(mockPort.foreningGetList(host)).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handle("handleTest", request.getHost(), mockPort::foreningGetList);

        // Assert
        verify(mockPort).foreningGetList(host);
    }

    @Test
    public void handleFunction_nullHost() {
        // Arrange
        Customer_Create request = factory.createCustomer_Create();
        Response mockResponse = factory.createResponse();
        when(mockPort.foreningGetList(null)).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handle("handleTest", request.getHost(), mockPort::foreningGetList);

        // Assert
        verify(mockPort).foreningGetList(null);
    }

    @Test
    public void handleSyncBiFunction() {
        // Arrange
        Internet_Services_GetProvisioningCodes request = factory.createInternet_Services_GetProvisioningCodes();

        Host host = factory.createHost();
        host.setResponseUrl("#");
        host.setUser("Test");
        host.setPassword("tseT");
        request.setHost(host);

        ResponseInternetProvisioningCodes mockResponse = factory.createResponseInternetProvisioningCodes();
        when(mockPort.internetServicesGetProvisioningCodes(eq(host), anyInt())).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handleSync("handleTest", request.getHost(), request.getInternetProvisioningCode(), mockPort::internetServicesGetProvisioningCodes);

        // Assert
        verify(mockPort).internetServicesGetProvisioningCodes(eq(host), anyInt());
    }

    @Test
    public void handleSyncBiFunction_nullHost() {
        // Arrange
        Internet_Services_GetProvisioningCodes request = factory.createInternet_Services_GetProvisioningCodes();

        ResponseInternetProvisioningCodes mockResponse = factory.createResponseInternetProvisioningCodes();
        when(mockPort.internetServicesGetProvisioningCodes(eq(null), anyInt())).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handleSync("handleTest", request.getHost(), request.getInternetProvisioningCode(), mockPort::internetServicesGetProvisioningCodes);

        // Assert
        verify(mockPort).internetServicesGetProvisioningCodes(eq(null), anyInt());
    }

    @Test
    public void handleSyncFunction() {
        // Arrange
        GetPriceListCodes request = factory.createGetPriceListCodes();

        Host host = factory.createHost();
        host.setResponseUrl("#");
        host.setUser("Test");
        host.setPassword("tseT");
        request.setHost(host);

        ResponsePriceListProvisioningCodes mockResponse = factory.createResponsePriceListProvisioningCodes();
        when(mockPort.getPriceListCodes(host)).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handleSync("handleTest", request.getHost(), mockPort::getPriceListCodes);

        // Assert
        verify(mockPort).getPriceListCodes(host);
    }

    @Test
    public void handleSyncFunction_nullHost() {
        // Arrange
        GetPriceListCodes request = factory.createGetPriceListCodes();

        ResponsePriceListProvisioningCodes mockResponse = factory.createResponsePriceListProvisioningCodes();
        when(mockPort.getPriceListCodes(null)).thenReturn(mockResponse);

        // Act
        proxyEndpoint.handleSync("handleTest", request.getHost(), mockPort::getPriceListCodes);

        // Assert
        verify(mockPort).getPriceListCodes(null);
    }

    @Test
    public void handleFallback() {
        // Arrange

        // Act
        Response response = proxyEndpoint.handleFallback("handleFallbackTest");

        // Assert
        verify(factory).createResponse();
        verify(factory).createArrayOfStatusDescription();
        verify(factory).createStatusDescription();
        assertEquals(1, response.getDescriptions().getStatusDescription().size());
        assertEquals(SERVICE_UNAVAILABLE_STATUS_CODE, response.getDescriptions()
                .getStatusDescription()
                .get(0)
                .getStatusCode());
    }

    @Test
    public void handleFallbackSync() {
        // Arrange

        // Act
        StatusDescription result = proxyEndpoint.handleFallbackSync("handleFallbackSyncTest");

        // Assert
        verify(factory).createStatusDescription();
        assertEquals(SERVICE_UNAVAILABLE_STATUS_CODE, result.getStatusCode());
    }
}