package com.novataris.response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ResponseClientApplication.class)
@WebAppConfiguration
@ActiveProfiles("local")
public class ResponseClientApplicationTests {

    @Test
    public void contextLoads() {
    }

}
