package com.novataris.response;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResponseClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResponseClientApplication.class, args);
    }
}
