package com.novataris.response.endpoints;

import com.novataris.proxy.generate.response.ActionCompleted;
import com.novataris.proxy.generate.response.ActionCompletedResponse;
import com.novataris.proxy.generate.response.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import java.util.Optional;

@Endpoint
public class ResponseEndpoint {

    private static final String NAMESPACE_URI = "SmileResponse";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ObjectFactory factory = new ObjectFactory();

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ActionCompleted")
    @ResponsePayload
    public ActionCompletedResponse actionCompleted(@RequestPayload ActionCompleted request) {
        ActionCompletedResponse response = factory.createActionCompletedResponse();
        response.setActionCompletedResult(true);
        logger.info("" + request.getData().getTransactionID() + ": " + request.getData().getDescription());
        if (request.getData().getXMLData().getContent().size() > 0) {
            Optional<Object> any = request.getData().getXMLData().getContent().stream().filter(o -> o instanceof Element).findAny();
            if (any.isPresent()) {
                LSSerializer lsSerializer = ((DOMImplementationLS) ((Element) any.get()).getOwnerDocument().getImplementation()).createLSSerializer();
                request.getData().getXMLData().getContent().forEach(o -> {
                    if (o instanceof Element) {
                        logger.info(lsSerializer.writeToString((Element) o));
                    }
                });
            }
        }
        return response;
    }
}
